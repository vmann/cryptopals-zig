const std = @import("std");

pub fn main() anyerror!void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer _ = gpa.deinit();
    const allocator = gpa.allocator();
    const stdin = std.io.bufferedReader(std.io.getStdIn().reader()).reader();
    const stdout = std.io.getStdOut().writer();

    const key = "ICE";
    const input = try stdin.readAllAlloc(allocator, 4096);
    defer allocator.free(input);

    var i: usize = 0;
    for (input) |*char| {
        char.* ^= key[i];
        i = (i + 1) % key.len;
    }

    try stdout.print("{s}\n", .{std.fmt.fmtSliceHexLower(input)});
}
