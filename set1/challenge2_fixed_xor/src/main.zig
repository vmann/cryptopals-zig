const std = @import("std");

fn readHex(allocator: std.mem.Allocator, reader: anytype, max_size: usize) !?[]u8 {
    var resultbuffer = std.ArrayList(u8).init(allocator);
    defer resultbuffer.deinit();

    while (true) {
        const line = try reader.readUntilDelimiterOrEofAlloc(allocator, '\n', max_size * 2);
        const hex = line orelse return null;
        defer allocator.free(hex);
        try resultbuffer.resize(hex.len / 2);
        _ = std.fmt.hexToBytes(resultbuffer.items, hex) catch {
            std.log.err("not a valid hexadecimal", .{});
            continue;
        };
        return resultbuffer.toOwnedSlice();
    }
}

pub fn main() anyerror!void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer _ = gpa.deinit();
    const allocator = gpa.allocator();
    const stdin = std.io.bufferedReader(std.io.getStdIn().reader()).reader();
    const stdout = std.io.getStdOut().writer();

    while (true) {
        const input1 = (try readHex(allocator, stdin, 4096)) orelse break;
        defer allocator.free(input1);

        const input2 = (try readHex(allocator, stdin, 4096)) orelse break;
        defer allocator.free(input2);

        if (input1.len != input2.len) {
            std.log.err("not the same length", .{});
            continue;
        }

        var output = try std.ArrayList(u8).initCapacity(allocator, input1.len);
        defer output.deinit();

        var i: usize = 0;
        while (i < input1.len) : (i += 1) {
            try output.append(input1[i] ^ input2[i]);
        }

        try stdout.print("{}\n", .{std.fmt.fmtSliceHexLower(output.items)});
    }
}
