const std = @import("std");

fn readHex(allocator: std.mem.Allocator, reader: anytype, max_size: usize) !?[]u8 {
    var resultbuffer = std.ArrayList(u8).init(allocator);
    defer resultbuffer.deinit();

    while (true) {
        const line = try reader.readUntilDelimiterOrEofAlloc(allocator, '\n', max_size * 2);
        const hex = line orelse return null;
        defer allocator.free(hex);
        try resultbuffer.resize(hex.len / 2);
        _ = std.fmt.hexToBytes(resultbuffer.items, hex) catch {
            std.log.err("not a valid hexadecimal", .{});
            continue;
        };
        return resultbuffer.toOwnedSlice();
    }
}

fn decrypt(plaintext: []u8, ciphertext: []const u8, key: u8) void {
    std.debug.assert(plaintext.len == ciphertext.len);
    var i: usize = 0;
    while (i < ciphertext.len) : (i += 1) {
        plaintext[i] = ciphertext[i] ^ key;
    }
}

fn score(plaintext: []const u8) u32 {
    const ranking = [_]u8{ 24, 7, 15, 17, 26, 11, 10, 19, 22, 4, 5, 16, 13, 21, 23, 8, 2, 18, 20, 25, 14, 6, 12, 3, 9, 1 };
    var s: u32 = 0;
    for (plaintext) |char| {
        if (char >= 'a' and char <= 'z') {
            s += ranking[char - 'a'];
        }
    }
    return s;
}

pub fn main() anyerror!void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer _ = gpa.deinit();
    const allocator = gpa.allocator();
    const stdin = std.io.bufferedReader(std.io.getStdIn().reader()).reader();
    const stdout = std.io.getStdOut().writer();

    while (true) {
        const ciphertext = (try readHex(allocator, stdin, 4096)) orelse break;
        defer allocator.free(ciphertext);

        var plaintext = try allocator.alloc(u8, ciphertext.len);
        defer allocator.free(plaintext);
        var bestkey: u8 = 0;
        var bestscore: u32 = 0;
        var key: u8 = 0;
        while (true) : (key += 1) {
            decrypt(plaintext, ciphertext, key);
            const s = score(plaintext);
            if (s > bestscore) {
                bestkey = key;
                bestscore = s;
            }
            key +%= 1;
            if (key == std.math.maxInt(u8)) {
                break;
            }
        }

        decrypt(plaintext, ciphertext, bestkey);
        try stdout.print("key: {x}\n{s}\n", .{ bestkey, plaintext });
    }
}
