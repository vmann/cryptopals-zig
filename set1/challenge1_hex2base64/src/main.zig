const std = @import("std");

pub fn main() anyerror!void {
    const stdin = std.io.bufferedReader(std.io.getStdIn().reader()).reader();
    const stdout = std.io.getStdOut().writer();

    var inputbuffer = std.mem.zeroes([1024]u8);
    var rawbuffer = std.mem.zeroes([512]u8);
    var outputbuffer = std.mem.zeroes([1024]u8);

    while (true) {
        const line = stdin.readUntilDelimiterOrEof(inputbuffer[0..], '\n') catch |err| {
            std.log.err("input failed: {s}\n", .{err});
            return;
        };
        const hex = line orelse return;
        const raw = std.fmt.hexToBytes(rawbuffer[0..], hex) catch {
            std.log.err("not a valid hexadecimal\n", .{});
            continue;
        };
        const base64 = std.base64.standard.Encoder.encode(outputbuffer[0..], raw);
        stdout.print("{s}\n", .{base64}) catch |err| {
            std.log.err("output failed: {s}\n", .{err});
            return;
        };
    }
}
